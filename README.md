CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Tax Number (tax_number) module provides a plugin system for tax numbers
verification that lets users define their own plugins. Out of the box
the module comes with two plugins for tax number validation: a portuguese
plugin and a spanish plugin. The module defines a new field type for data
storage and a webform element with the same plugin architecture

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

 * For field types in the form display settings,
    simply choose which validator you want to use

 * For webform elements chose the validation in the element settings.

MAINTAINERS
-----------

Current maintainers:
 * Nelson Alves (nsalves) - https://www.drupal.org/u/nsalves

This project has been sponsored by:
 * NTT DATA
   NTT DATA – a part of NTT Group – is a trusted global innovator of
   IT and business services headquartered in Tokyo.
   NTT is one of the largest IT services provider in the world
   and has 140,000 professionals, operating in more than 50 countries.
   NTT DATA supports clients in their digital development
   through a wide range of consulting and strategic advisory services,
   cutting-edge technologies, applications,
   infrastructure, modernization of IT and BPOs.
   We contribute with vast experience in all sectors of
   economic activity and have extensive 
   knowledge of the locations in which we operate.
