<?php

namespace Drupal\tax_number\Plugin\WebformElement;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;

/**
 * Provides a 'webform_tax_number_element' element.
 *
 * @WebformElement(
 *   id = "webform_tax_number_element",
 *   label = @Translation("Webform tax number element"),
 *   description = @Translation("Provides a webform tax number element"),
 *   category = @Translation("Advanced elements"),
 * )
 *
 * @see \Drupal\webform_example_element\Element\WebformExampleElement
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformTaxNumberElement extends WebformElementBase {

  /**
   * The tax_number widget manager.
   *
   * @var \Drupal\tax_number\Plugin\TaxNumberWidgetManager
   */
  protected $taxNumberWidgetManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->taxNumberWidgetManager = $container->get('plugin.manager.tax_number.widget');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      'multiple' => '',
      'size' => '',
      'minlength' => '',
      'maxlength' => '',
      'placeholder' => '',
      'tax_number_validator' => '',
    ] + parent::defineDefaultProperties();
  }

  /* ************************************************************************ */

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['tax_number_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Tax Number settings'),
    ];

    // Find available tax number widgets.
    $tax_number_widgets = [];

    foreach ($this->taxNumberWidgetManager->getTaxnumberWidgets() as $tax_number_widget) {
      $tax_number_widgets[$tax_number_widget['id']] = $tax_number_widget['label'];
    }

    $form['tax_number_settings']['tax_number_validator'] = [
      '#title' => $this->t('Tax Number validator'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $tax_number_widgets,
      '#description' => $this->t('Choose the tax number validator plugin that corresponds to your website requirements.'),
    ];

    return $form;
  }

}
