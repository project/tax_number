<?php

namespace Drupal\tax_number\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Plugin manager for finding and using tax number widgets.
 */
class TaxNumberWidgetManager extends DefaultPluginManager {

  /**
   * Constructs a TaxNumberWidgetManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/TaxNumber/Widget', $namespaces, $module_handler, 'Drupal\tax_number\Plugin\TaxNumberWidgetInterface', 'Drupal\tax_number\Annotation\TaxNumberWidget');
    $this->alterInfo('tax_number_info');
    $this->setCacheBackend($cache_backend, 'tax_number_info_plugins');
  }

  /**
   * Function for getplugin ID.
   */
  public function getPlugin($plugin_id = 'default_widget') {
    return $this->createInstance($plugin_id);
  }

  /**
   * Get tax_number widgets to populate select in Tax Number form.
   */
  public function getTaxNumberWidgets() {
    $plugin_definitions = $this->getDefinitions();
    usort($plugin_definitions, function ($definition1, $definition2) {
      return $definition1['weight'] <=> $definition2['weight'];
    });

    return $plugin_definitions;
  }

  /**
   * Validate tax number handler.
   *
   * @param string $widget
   *   Tax Number widget machine name.
   *
   * @return bool
   *   True if tax number is valid, otherwise return false.
   */
  public function validateTaxNumber($widget) {
    if (empty($widget)) {
      $widget = 'default_widget';
    }

    return $this->getPlugin($widget)->getPlugin()->validateTaxNumber();
  }

}
