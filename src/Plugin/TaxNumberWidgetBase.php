<?php

namespace Drupal\tax_number\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base for a tax_number widget.
 */
abstract class TaxNumberWidgetBase extends PluginBase implements TaxNumberWidgetInterface, ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * Return the plugin.
   */
  public function getPlugin() {
    return $this;
  }

  /**
   * Get the plugin ID.
   */
  public function getId() {
    return $this->pluginDefinition['id'];
  }

  /**
   * Get the plugin label.
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * Get the plugin weight.
   */
  public function getWeight() {
    return $this->pluginDefinition['weight'];
  }

}
