<?php

namespace Drupal\tax_number\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Interface for the tax number widget.
 */
interface TaxNumberWidgetInterface extends PluginInspectionInterface {

  /**
   * Validate tax number handler.
   *
   * @return bool
   *   True if tax number is valid, otherwise return false.
   */
  public function validateTaxNumber($value);

}
