<?php

namespace Drupal\tax_number\Plugin\TaxNumber\Widget;

use Drupal\tax_number\Plugin\TaxNumberWidgetBase;

/**
 * Provides a widget for spanish tax number.
 *
 * @TaxNumberWidget(
 *   id = "es_widget",
 *   label = @Translation("Spanish"),
 *   weight = 0
 * )
 */
class SpanishWidget extends TaxNumberWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function validateTaxNumber($value) {
    if ($value) {
      $nif = trim($value);
      $cif = strtoupper($nif);
      for ($i = 0; $i < 9; $i++) {
        $num[$i] = substr($cif, $i, 1);
      }

      if (!preg_match('/((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)/', $cif)) {
        return FALSE;
      }

      if (preg_match('/(^[0-9]{8}[A-Z]{1}$)/', $cif)) {
        if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE', substr($cif, 0, 8) % 23, 1)) {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }

      $sum = $num[2] + $num[4] + $num[6];
      for ($i = 1; $i < 8; $i += 2) {
        $sum += intval(substr((2 * $num[$i]), 0, 1)) + intval(substr((2 * $num[$i]), 1, 1));
      }
      $n = 10 - substr($sum, strlen($sum) - 1, 1);

      if (preg_match('/^[KLM]{1}/', $cif)) {
        if ($num[8] == chr(64 + $n)) {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }

      if (preg_match('/^[ABCDEFGHJNPQRSUVW]{1}/', $cif)) {
        if ($num[8] == chr(64 + $n) || $num[8] == substr($n, strlen($n) - 1, 1)) {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }

      if (preg_match('/^[T]{1}/', $cif)) {
        if ($num[8] == preg_match('^[T]{1}[A-Z0-9]{8}$', $cif)) {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }

      if (preg_match('/^[XYZ]{1}/', $cif)) {
        if ($num[8] == substr('TRWAGMYFPDXBNJZSQVHLCKE',
        substr(str_replace(['X', 'Y', 'Z'],
        ['0', '1', '2'], $cif), 0, 8) % 23, 1)) {
          return TRUE;
        }
        else {
          return FALSE;
        }
      }

      return FALSE;
    }
  }

}
