<?php

namespace Drupal\tax_number\Plugin\TaxNumber\Widget;

use Drupal\tax_number\Plugin\TaxNumberWidgetBase;

/**
 * Provides a widget for portuguese nif.
 *
 * @TaxNumberWidget(
 *   id = "pt_widget",
 *   label = @Translation("Portuguese"),
 *   weight = 0
 * )
 */
class PortugueseWidget extends TaxNumberWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function validateTaxNumber($value) {
    if ($value) {
      $nif = trim($value);
      $nif_split = str_split($nif);
      $nif_first_number = [1, 2, 3, 5, 6, 7, 8, 9];
      if (is_numeric($nif) && strlen($nif) == 9 && in_array($nif_split[0], $nif_first_number)) {
        $check_digit = 0;
        for ($i = 0; $i < 8; $i++) {
          $check_digit += $nif_split[$i] * (10 - $i - 1);
        }
        $check_digit = 11 - ($check_digit % 11);
        $check_digit = $check_digit >= 10 ? 0 : $check_digit;
        if ($check_digit == $nif_split[8]) {
          return TRUE;
        }
      }
      return FALSE;
    }
  }

}
