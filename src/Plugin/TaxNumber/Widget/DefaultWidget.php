<?php

namespace Drupal\tax_number\Plugin\TaxNumber\Widget;

use Drupal\tax_number\Plugin\TaxNumberWidgetBase;

/**
 * Provides a default widget.
 *
 * @TaxNumberWidget(
 *   id = "default_widget",
 *   label = @Translation("Default"),
 *   weight = -100
 * )
 */
class DefaultWidget extends TaxNumberWidgetBase {

  /**
   * {@inheritdoc}
   */
  public function validateTaxNumber($value) {
    return TRUE;
  }

}
