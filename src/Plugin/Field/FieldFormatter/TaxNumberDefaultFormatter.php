<?php

namespace Drupal\tax_number\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'tax_number_default' formatter.
 *
 * @FieldFormatter(
 *   id = "tax_number_default",
 *   label = @Translation("Tax Number"),
 *   field_types = {
 *     "tax_number"
 *   }
 * )
 */
class TaxNumberDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      // Render each element as markup.
      $element[$delta] = ['#markup' => $item->value];
    }

    return $element;
  }

}
