<?php

namespace Drupal\tax_number\Plugin\Field\FieldWidget;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'tax_number' widget.
 *
 * @FieldWidget(
 *   id = "tax_number_default",
 *   label = @Translation("Tax Number field"),
 *   field_types = {
 *     "tax_number",
 *   }
 * )
 */
class TaxNumberWidget extends WidgetBase {

  /**
   * The tax_number widget manager.
   *
   * @var \Drupal\tax_number\Plugin\TaxNumberWidgetManager
   */
  protected $taxNumberWidgetManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->taxNumberWidgetManager = $container->get('plugin.manager.tax_number.widget');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'placeholder' => '',
      'tax_number_validator' => 'default_widget',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => $this->t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    // Find available tax_number widgets.
    $tax_number_widgets = [];

    foreach ($this->taxNumberWidgetManager->getTaxNumberWidgets() as $tax_number_widget) {
      $tax_number_widgets[$tax_number_widget['id']] = $tax_number_widget['label'];
    }

    $element['tax_number_validator'] = [
      '#title' => $this->t('Tax Number validator'),
      '#default_value' => $this->getSetting('tax_number_validator'),
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $tax_number_widgets,
      '#description' => $this->t('Choose the tax number validator plugin that corresponds to your website requirements.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $placeholder = $this->getSetting('placeholder');
    if (!empty($placeholder)) {
      $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $placeholder]);
    }
    else {
      $summary[] = $this->t('No placeholder');
    }

    $tax_number_validator = $this->getSetting('tax_number_validator');
    if ($tax_number_validator) {
      $pluginManager = $this->taxNumberWidgetManager;
      $pluginLabel = $pluginManager->createInstance($tax_number_validator)->getLabel()->__toString();
      $summary[] = $this->t('Tax Number Validator: @placeholder', ['@placeholder' => $pluginLabel]);
    }
    else {
      $summary[] = $this->t('Default');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = $items[$delta]->value ?? '';

    $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#size' => 30,
      '#maxlength' => 9,
      '#placeholder' => $this->getSetting('placeholder'),
      '#tax_number_validator' => $this->getSetting('tax_number_validator'),
      '#element_validate' => [
        [static::class, 'validateTaxNumber'],
      ],
    ];
    return ['value' => $element];
  }

  /**
   * Validate the tax number number field.
   */
  public static function validateTaxNumber($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    $pluginManager = \Drupal::service('plugin.manager.tax_number.widget');
    $validation = $pluginManager->createInstance($element['#tax_number_validator'])->validateTaxNumber($value);
    if (!$validation) {
      $form_state->setError($element, t("The Tax Number is not valid"));
    }
  }
}
