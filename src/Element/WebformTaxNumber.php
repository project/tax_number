<?php

namespace Drupal\tax_number\Element;

use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\FormElement;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'webform_tax_number_element'.
 *
 * Webform elements are just wrappers around form elements, therefore every
 * webform element must have correspond FormElement.
 *
 * Below is the definition for a custom 'webform_example_element' which just
 * renders a simple text field.
 *
 * @FormElement("webform_tax_number_element")
 *
 * @see \Drupal\Core\Render\Element\FormElement
 * @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21Element%21FormElement.php/class/FormElement
 * @see \Drupal\Core\Render\Element\RenderElement
 * @see https://api.drupal.org/api/drupal/namespace/Drupal%21Core%21Render%21Element
 * @see \Drupal\webform_example_element\Element\WebformExampleElement
 */
class WebformTaxNumber extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#size' => 30,
      '#maxlength' => 9,
      '#process' => [
        [$class, 'processWebformElementTaxNumber'],
        [$class, 'processAjaxForm'],
      ],
      '#element_validate' => [
        [$class, 'validateWebformTaxNumberElement'],
      ],
      '#pre_render' => [
        [$class, 'preRenderWebformTaxNumberElement'],
      ],
      '#theme' => 'input__webform_tax_number_element',
      '#theme_wrappers' => ['form_element'],
    ];
  }

  /**
   * Processes a 'webform_tax_number_element' element.
   */
  public static function processWebformElementTaxNumber(&$element, FormStateInterface $form_state, &$complete_form) {

    return $element;
  }

  /**
   * Webform element validation handler for #type 'webform_tax_number_element'.
   */
  public static function validateWebformTaxNumberElement(&$element, FormStateInterface $form_state, &$complete_form) {
    $value = $element['#value'];
    $pluginManager = \Drupal::service('plugin.manager.tax_number.widget');
    $validation = $pluginManager->createInstance($element['#tax_number_validator'])->validateTaxNumber($value);
    if (!$validation) {
      $form_state->setError($element, t("The Tax number is not valid"));
    }
  }

  /**
   * Prepares a #type 'text' render element for theme_element().
   *
   * @param array $element
   *   An associative array containing the properties of the element.
   *   Properties used: #title, #value, #description, #size, #maxlength,
   *   #placeholder, #required, #attributes.
   *
   * @return array
   *   The $element with prepared variables ready for theme_element().
   */
  public static function preRenderWebformTaxNumberElement(array $element) {
    $element['#attributes']['type'] = 'text';
    Element::setAttributes($element, [
      'id',
      'name',
      'value',
      'size',
      'maxlength',
      'placeholder',
    ]);
    static::setAttributes($element, ['form-text', 'webform-tax-number-element']);
    return $element;
  }

}
